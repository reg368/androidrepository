package james.com.imageproduct;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.ImageView;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by R30 on 2016/1/30.
 */
public class ImageCompress {

    private DisplayMetrics displaymetrics;
    private Context context;

    public ImageCompress(Context context){
        this.context = context;
        displaymetrics = new DisplayMetrics();
        ((Activity)context).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
    }


    public Bitmap  getImage(String url)throws Exception{
        return downloagImageTask(url,"","",0);
    }

    public Bitmap  getImage(String url,String name,String password)throws Exception{
        return downloagImageTask(url,name,password,0);
    }

    public Bitmap  getImage(String url,int size)throws Exception{
        return downloagImageTask(url,"","",size);
    }

    public Bitmap  getImage(String str,String name,String password,int size)throws Exception{
        return downloagImageTask(str,name,password,size);
    }

    private Bitmap downloagImageTask(String str,String name,String password,int size)throws Exception{
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(context.getString(R.string.ImageLoadingTask));
        progressDialog.setIndeterminate(true);

        if (!progressDialog.isShowing())
            progressDialog.show();

        URL url = new URL(str);
        URLConnection uc = url.openConnection();
        String userpass = name + ":" + password;
        String basicAuth = "Basic " + new String(android.util.Base64.encode(userpass.getBytes(), android.util.Base64.URL_SAFE));
        uc.setRequestProperty ("Authorization", basicAuth);
        InputStream in = null;
        InputStream tempIn = null;
        Bitmap pic = null;
        try {
            in = uc.getInputStream();
            tempIn = uc.getInputStream();
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(tempIn,null,options);
            int picH = options.outHeight;
            int picW = options.outWidth;

            if(in != null){
                options = new BitmapFactory.Options();
                //有指定大小
                if(size != 0){
                    int[] newSize = calculateSizeTest(picW,picH,size);
                    options.inSampleSize=calculateScale(picW,picH,newSize[0],newSize[1]);
                    pic = BitmapFactory.decodeStream(in, null, options);
                    pic = Bitmap.createScaledBitmap(pic,newSize[0],newSize[1],false);
                }else{
                    if(picH > displaymetrics.heightPixels || picW > displaymetrics.widthPixels){
                        //取得原裝置 寬或高 最大值 來進行邊界值壓縮
                        int screenMaxPixels = Math.max(displaymetrics.heightPixels,displaymetrics.widthPixels);
                        //newSize 儲存新換算出來的寬和高
                        int[] newSize = resize(picW,picH,screenMaxPixels,displaymetrics);
                        //BitmapFactory.decodeStream 照著壓縮比 去取得原圖後的壓縮 才不會OOM
                        options.inSampleSize=calculateScale(picW,picH,newSize[0],newSize[1]);
                        pic =  BitmapFactory.decodeStream(in,null, options);
                        //取得之後依照換算出來需要的長寬去修改
                        pic = Bitmap.createScaledBitmap(pic,newSize[0],newSize[1],false);
                    }else{
                        pic = BitmapFactory.decodeStream(in);
                    }
                }
            }
        }catch (Exception e){
            Log.e("Error", e.getMessage());
            return null;
        }

        if (progressDialog != null)
            progressDialog.cancel();

        return pic;
    }

    private int calculateScale(int picW,int picH,int newW,int newH){
        int scale=1;
        //將算出來要取得的長寬去計算壓縮比要多少
        while(true){
            Log.d("while","while: "+scale);
            if(picW/2<newW || picH/2<newH)
                break;
            picW/=2;
            picH/=2;
            scale++;
        }
        return scale;
    }

    private int[] resize(int picW,int picH,int resizeScale,DisplayMetrics displaymetrics){
        //先依照目前裝置螢幕寬或高(取最大的值)當壓縮比例 , 去換算縮圖後的長寬要多少
        int[] newSize =calculateSizeTest( picW, picH,resizeScale);
        //看壓縮幾次用 Log
        int  count = 0;
        //如果縮圖過後的圖 , 長或寬還是超過目前的螢幕的大小的話
        //進入迴圈並把壓縮比減少一 , 逐步壓縮直到長寬值是裝置螢幕可容許為止
        while(newSize[0] > displaymetrics.widthPixels || newSize[1] > displaymetrics.heightPixels){
            newSize = calculateSizeTest(newSize[0],newSize[1],resizeScale--);
            count++;
        }
        return newSize;
    }

    private int[] calculateSizeTest(int imgWidth,int imgHeight,int resizeScale){
        double partion = imgWidth*1.0/imgHeight;
        double sqrtLength = Math.sqrt(partion*partion + 1);
        //新的缩略图大小
        double newImgW = resizeScale*(partion / sqrtLength);
        double newImgH = resizeScale*(1 / sqrtLength);
        float scaleW = (float) (newImgW/imgWidth);
        float scaleH = (float) (newImgH/imgHeight);

        RectF dstR = new RectF(0, 0, imgWidth, imgHeight);
        Matrix mx = new Matrix();
        //对原图片进行缩放
        mx.postScale(scaleW, scaleH);
        RectF deviceR = new RectF();
        mx.mapRect(deviceR, dstR);

        int neww = Math.round(deviceR.width());
        int newh = Math.round(deviceR.height());
        Log.d("AttractionPic","neww : "+ neww);
        Log.d("AttractionPic", "newh : " + newh);
        int[] newSize = {neww,newh};

        return newSize;
    }


}
