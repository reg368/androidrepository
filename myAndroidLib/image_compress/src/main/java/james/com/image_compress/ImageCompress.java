package james.com.image_compress;

/**
 * Created by R30 on 2016/2/1.
 */
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.os.AsyncTask;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by R30 on 2016/1/30.
 */
public class ImageCompress {

    private DisplayMetrics displaymetrics;
    private Context context;

    public ImageCompress(Context context){
        this.context = context;
        displaymetrics = new DisplayMetrics();
        ((Activity)context).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
    }


    public void  getImage(ImageView view,String url)throws Exception{
       new DownloadImageAsync(view,"","",0).execute(url);
    }

    public void  getImage(ImageView view,String url,String name,String password)throws Exception{
        new DownloadImageAsync(view,name,password,0).execute(url);
    }

    public void  getImage(ImageView view,String url,int size)throws Exception{
        new DownloadImageAsync(view,"","",size).execute(url);
    }

    public void  getImage(ImageView view,String url,String name,String password,int size)throws Exception{
        new DownloadImageAsync(view,name,password,size).execute(url);
    }

    private class DownloadImageAsync extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;
        String user;
        String pwd;
        int size;

        private ProgressDialog progressDialog = null;

        public DownloadImageAsync(ImageView bmImage,String user ,String pwd, int size) {
            this.bmImage = bmImage;
            this.user = user;
            this.pwd = pwd;
            this.size = size;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            if(progressDialog == null) {
                progressDialog = new ProgressDialog(context);
                progressDialog.setMessage(context.getString(R.string.ImageLoadingTask));
                progressDialog.setIndeterminate(true);
                if (!progressDialog.isShowing())
                    progressDialog.show();
            }
        }

        protected Bitmap doInBackground(String... urls) {
            try {
                return downloagImageTask(urls[0], user, pwd, size);
            }catch (Exception e){
                Log.d("exception",e.getMessage());
                return null;
            }
        }

        protected  void onPostExecute(Bitmap result) {
            if(result != null) {
                this.bmImage.setImageBitmap(result);
                if (progressDialog != null)
                    progressDialog.cancel();
            }

        }

    }

    private Bitmap downloagImageTask(String str,String name,String password,int size)throws Exception{


        InputStream in = getImageUrlWithAuth(str,name,password);
        InputStream tempIn = getImageUrlWithAuth(str,name,password);
        Bitmap pic = null;

        try {

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(tempIn,null,options);
            int picH = options.outHeight;
            int picW = options.outWidth;
            if(in != null){
                options = new BitmapFactory.Options();

                if(size != 0){
                    int[] newSize = calculateSizeTest(picW,picH,size);
                    options.inSampleSize=calculateScale(picW,picH,newSize[0],newSize[1]);
                    pic = BitmapFactory.decodeStream(in, null, options);
                    pic = Bitmap.createScaledBitmap(pic,newSize[0],newSize[1],false);
                }else{
                    if(picH > displaymetrics.heightPixels || picW > displaymetrics.widthPixels){

                        int screenMaxPixels = Math.max(displaymetrics.heightPixels,displaymetrics.widthPixels);

                        int[] newSize = resize(picW,picH,screenMaxPixels,displaymetrics);

                        options.inSampleSize=calculateScale(picW,picH,newSize[0],newSize[1]);

                        pic =  BitmapFactory.decodeStream(in,null, options);

                        pic = Bitmap.createScaledBitmap(pic, newSize[0], newSize[1], false);

                    }else{
                        pic = BitmapFactory.decodeStream(in);
                    }
                }
            }
        }catch (Exception e){
            Log.e("Error", e.getMessage());
            return null;
        }


        return pic;
    }

    private int calculateScale(int picW,int picH,int newW,int newH){
        int scale=1;

        while(true){
            Log.d("while","while: "+scale);
            if(picW/2<newW || picH/2<newH)
                break;
            picW/=2;
            picH/=2;
            scale++;
        }
        return scale;
    }

    private int[] resize(int picW,int picH,int resizeScale,DisplayMetrics displaymetrics){

        int[] newSize =calculateSizeTest( picW, picH,resizeScale);

        int  count = 0;
        while(newSize[0] > displaymetrics.widthPixels || newSize[1] > displaymetrics.heightPixels){
            newSize = calculateSizeTest(newSize[0],newSize[1],resizeScale--);
            count++;
        }
        return newSize;
    }

    private int[] calculateSizeTest(int imgWidth,int imgHeight,int resizeScale){
        double partion = imgWidth*1.0/imgHeight;
        double sqrtLength = Math.sqrt(partion*partion + 1);

        double newImgW = resizeScale*(partion / sqrtLength);
        double newImgH = resizeScale*(1 / sqrtLength);
        float scaleW = (float) (newImgW/imgWidth);
        float scaleH = (float) (newImgH/imgHeight);

        RectF dstR = new RectF(0, 0, imgWidth, imgHeight);
        Matrix mx = new Matrix();

        mx.postScale(scaleW, scaleH);
        RectF deviceR = new RectF();
        mx.mapRect(deviceR, dstR);

        int neww = Math.round(deviceR.width());
        int newh = Math.round(deviceR.height());
        Log.d("AttractionPic","neww : "+ neww);
        Log.d("AttractionPic", "newh : " + newh);
        int[] newSize = {neww,newh};

        return newSize;
    }


    private InputStream getImageUrlWithAuth(String str,String name,String password)throws Exception{
        URL url = new URL(str);
        URLConnection uc = url.openConnection();
        String userpass = name + ":" + password;
        String basicAuth = "Basic " + new String(android.util.Base64.encode(userpass.getBytes(), android.util.Base64.URL_SAFE));
        uc.setRequestProperty("Authorization", basicAuth);
        InputStream in = null;
        try {
            in = uc.getInputStream();
        }catch (Exception e){
            Log.e("Error", e.getMessage());
            return null;
        }
        return in;
    }

}
